# Changelog

## [Unreleased]

### Added

### Changed

- Improves UX on setting up Duo Plugin
  ([#63](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/issues/63))

### Removed

### Fixed

- Ensure plugin restart when installing or updating

## [0.1.2] - 2023-08-15

### Changed

- Rename plugin to GitLab
  Duo ([#55](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/issues/55))

### Fixed

- Status indicator is not getting refreshed while adding/removing
  token ([#36](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/issues/36))
- GitLab Instance URL with dash(es) not accepted by the
  plugin ([#59](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/issues/59))
  by [@Prezu](https://gitlab.com/Prezu)
- Handle offline exceptions and show them in the
  UI ([!41](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/merge_requests/41))
- Plugin breaks in some IDEs due to wrong imports ([#61](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/issues/61))

## [0.1.1] - 2023-07-21

### Added

- Settings screen to configure a GitLab instance and credentials for the Plugin
- Status bar icon to track configuration and busy/ready state
- GitLab Duo Code Suggestions initial integration

[Unreleased]: https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/compare/v0.1.2...HEAD
[0.1.2]: https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/compare/v0.1.1...v0.1.2
[0.1.1]: https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/commits/v0.1.1
