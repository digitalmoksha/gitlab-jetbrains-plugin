package com.gitlab.plugin.ui

import com.gitlab.plugin.api.GitLabOfflineException
import com.gitlab.plugin.api.GitLabUnauthorizedException
import com.gitlab.plugin.api.duo.ExceptionHandler
import com.intellij.openapi.diagnostic.Logger

class CreateNotificationExceptionHandler(
  private val notificationManager: GitLabNotificationManager = GitLabNotificationManager(),
  private val logger: Logger = Logger.getInstance(CreateNotificationExceptionHandler::class.java)
) :
  ExceptionHandler {
  companion object {
    const val UNAUTHORIZED_NOTIFICATION_MESSAGE = "User is unauthorized to use code suggestions."
    const val OFFLINE_NOTIFICATION_MESSAGE = "Cannot reach GitLab server. It appears to be offline."
  }

  private var showOfflineNotification = true
  private var showAuthNotification = true

  override fun handleException(exception: Throwable) = when (exception) {
    is GitLabUnauthorizedException -> if (showAuthNotification) {
      logger.error(exception)
      sendNotification(UNAUTHORIZED_NOTIFICATION_MESSAGE)
      showAuthNotification = false
    } else {
    }

    is GitLabOfflineException -> if (showOfflineNotification) {
      logger.error(exception)
      sendNotification(OFFLINE_NOTIFICATION_MESSAGE)
      showOfflineNotification = false
    } else {
    }

    else -> {
      super.handleException(exception)
    }
  }

  private fun sendNotification(message: String) {
    notificationManager.sendNotification(Notification("GitLab", message), null)
  }

  fun resetNotifications() {
    showOfflineNotification = true
    showAuthNotification = true
  }
}
