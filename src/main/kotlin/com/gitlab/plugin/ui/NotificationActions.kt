package com.gitlab.plugin.ui

import com.gitlab.plugin.GitLabBundle.message
import com.gitlab.plugin.GitLabBundle.openPluginSettings
import com.intellij.openapi.project.Project

data class NotificationAction(val title: String, val run: (dismiss: () -> Unit) -> Unit) {
  companion object {
    fun settings(project: Project, title: String = message("notification-action.settings")) = NotificationAction(title) { dismiss ->
      dismiss()

      openPluginSettings(project)
    }
  }
}
