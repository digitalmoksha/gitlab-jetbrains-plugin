package com.gitlab.plugin

import com.gitlab.plugin.authentication.GitLabPersistentAccount
import com.gitlab.plugin.util.TokenUtil
import com.intellij.ide.BrowserUtil
import com.intellij.openapi.options.BoundConfigurable
import com.intellij.openapi.options.ConfigurationException
import com.intellij.openapi.ui.DialogPanel
import com.intellij.ui.dsl.builder.bindText
import com.intellij.ui.dsl.builder.panel
import java.util.*

class GitLabSettingsConfigurable : BoundConfigurable(GitLabBundle.message("settings.ui.group.name")) {

  private var tokenText = TokenUtil.getToken() ?: ""
  private val bundle: ResourceBundle = ResourceBundle.getBundle("messages.GitLabBundle")

  private val urlErrorMessage = bundle.getString("settings.ui.error.url.invalid")
  private val tokenLabel = bundle.getString("settings.ui.gitlab.token")
  private val createTokenLabel = bundle.getString("settings.ui.gitlab.create-token")
  private val hostLabel = bundle.getString("settings.ui.gitlab.url")
  private val docsLabel = bundle.getString("settings.ui.gitlab.docs-link-label")

  private var settingsPanel: DialogPanel? = null

  override fun createPanel(): DialogPanel {
    val account = GitLabPersistentAccount.getInstance()
    var url = account.state.url

    settingsPanel = panel {
      row {
        browserLink(docsLabel, DOCS_URL)
      }
      separator()
      row(hostLabel) {
        textField().bindText(account.state::url)
          .addValidationRule(urlErrorMessage) { it.text.isBlank() }
          .onChanged { component -> url = component.text }
      }
      row(tokenLabel) {
        passwordField()
          .bindText(::tokenText)
          .focused()
        button(createTokenLabel) { openUrl(url, CREATE_TOKEN_URL) }
      }
    }

    return settingsPanel as DialogPanel
  }

  override fun apply() {
    val panel = settingsPanel ?: return
    val validationMessages = panel.validateAll()

    if (validationMessages.isEmpty()) {
      super.apply()
      TokenUtil.setToken(tokenText)

      notifyChange(tokenText)
    } else {
      throw ConfigurationException(validationMessages.joinToString(separator = "\n") { it.message })
    }
  }

  private fun notifyChange(token: String) {
    val account = GitLabPersistentAccount.getInstance()
    account.state.enabled = token.isNotEmpty()
    // TODO: Add notification publisher code here
  }

  private fun openUrl(host: String, endpoint: String) {
    BrowserUtil.open("$host$endpoint")
  }

  companion object {
    private const val CREATE_TOKEN_URL =
      "/-/profile/personal_access_tokens?name=GitLab%20Duo%20For%20JetBrains&scopes=api"

    private const val DOCS_URL =
      "https://docs.gitlab.com/ee/user/project/repository/code_suggestions.html#enable-code-suggestions-on-gitlab-saas"
  }
}
