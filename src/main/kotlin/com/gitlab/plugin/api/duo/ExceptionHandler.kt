package com.gitlab.plugin.api.duo

interface ExceptionHandler {
  fun handleException(exception: Throwable) {
    throw exception
  }
}
