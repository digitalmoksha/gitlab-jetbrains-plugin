package com.gitlab.plugin.api.duo

import com.gitlab.plugin.api.duo.requests.Completion
import io.ktor.client.call.*
import io.ktor.client.request.*

class DuoApi(
  private val client: DuoClient = DuoClient()
) {
  companion object {
    val instance by lazy { DuoApi() }
  }

  @Deprecated("Endpoint deprecated on GitLab server versions < 16.3")
  suspend fun completionsLegacy(payload: Completion.Payload): Completion.Response? {
    if (!client.isEnabled()) return null

    return client
      .modelGatewayPost { setBody(payload) }
      ?.body<Completion.Response?>()
  }

  fun isApiEnabled(): Boolean = client.isEnabled()
}
