package com.gitlab.plugin.api.duo

import com.gitlab.plugin.api.GitLabOfflineException
import com.gitlab.plugin.api.GitLabUnauthorizedException
import com.gitlab.plugin.api.GitlabStatusPlugin
import com.gitlab.plugin.ui.CreateNotificationExceptionHandler
import com.gitlab.plugin.ui.IconStatusModifier
import com.gitlab.plugin.util.GitLabUtil
import com.google.gson.FieldNamingPolicy
import io.ktor.client.*
import io.ktor.client.engine.*
import io.ktor.client.engine.cio.*
import io.ktor.client.network.sockets.*
import io.ktor.client.plugins.*
import io.ktor.client.plugins.contentnegotiation.*
import io.ktor.client.request.*
import io.ktor.client.statement.*
import io.ktor.http.*
import io.ktor.serialization.gson.*
import java.nio.channels.UnresolvedAddressException

class DuoClient(
  private val tokenProvider: TokenProvider = PatProvider(),
  private val jwtProvider: TokenProvider = JwtProvider(),
  private val exceptionHandler: ExceptionHandler = CreateNotificationExceptionHandler(),
  userAgent: String = GitLabUtil.userAgent,
  shouldRetry: Boolean = true,
  httpClientEngine: HttpClientEngine = CIO.create(),
  onStatusChanged: DuoClientRequestListener = IconStatusModifier(),
) {
  fun interface TokenProvider {
    fun token(): String
  }

  interface DuoClientRequestListener {
    fun onRequestStart() {}
    fun onRequestSuccess() {}
    fun onRequestError() {}
  }

  companion object {
    const val MODEL_GATEWAY_ENDPOINT = "https://codesuggestions.gitlab.com/v2/completions"
  }

  val restClient = HttpClient(httpClientEngine) {
    expectSuccess = true

    defaultRequest {
      contentType(ContentType.Application.Json)
    }
    install(GitlabStatusPlugin) {
      statusCallback = onStatusChanged
    }
    install(UserAgent) {
      agent = userAgent
    }
    if (shouldRetry) {
      install(HttpRequestRetry) {
        retryOnServerErrors(maxRetries = 3)
        exponentialDelay()
      }
    }
    install(ContentNegotiation) {
      gson {
        setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
      }
    }

    HttpResponseValidator {
      handleResponseExceptionWithRequest { cause: Throwable, request: HttpRequest ->
        onStatusChanged.onRequestError()
        
        when (cause) {
          is ClientRequestException -> {
            val exceptionResponse = cause.response

            if (exceptionResponse.status == HttpStatusCode.Unauthorized) {
              val exceptionResponseText = exceptionResponse.bodyAsText()
              throw GitLabUnauthorizedException(exceptionResponse, exceptionResponseText)
            } else {
              throw cause
            }
          }

          is UnresolvedAddressException -> throw GitLabOfflineException(request, cause)
          is ConnectTimeoutException -> throw GitLabOfflineException(request, cause)
          else -> throw cause
        }
      }
    }
  }

  fun isEnabled(): Boolean = tokenProvider.token().isNotEmpty()

  suspend fun get(endpoint: String, block: HttpRequestBuilder.() -> Unit = {}): HttpResponse? = request(endpoint) {
    method = HttpMethod.Get
    header("Authorization", "Bearer ${tokenProvider.token()}")
    block.invoke(this)
  }

  suspend fun post(endpoint: String, block: HttpRequestBuilder.() -> Unit = {}): HttpResponse? = request(endpoint) {
    method = HttpMethod.Post
    header("Authorization", "Bearer ${tokenProvider.token()}")
    block.invoke(this)
  }

  @Deprecated("Endpoint deprecated on GitLab server versions < 16.3")
  suspend fun modelGatewayPost(block: HttpRequestBuilder.() -> Unit = {}): HttpResponse? =
    request(MODEL_GATEWAY_ENDPOINT) {
      method = HttpMethod.Post
      header("Authorization", "Bearer ${jwtProvider.token()}")
      header("X-Gitlab-Authentication-Type", "oidc")
      block.invoke(this)
    }

  private suspend fun request(endpoint: String, block: HttpRequestBuilder.() -> Unit = {}): HttpResponse? {
    try {
      return restClient.request(endpoint, block)
    } catch (ex: Exception) {
      exceptionHandler.handleException(ex)
    }

    return null
  }
}
