package com.gitlab.plugin.api.duo

import com.gitlab.plugin.util.TokenUtil

class PatProvider : DuoClient.TokenProvider {
  override fun token(): String = TokenUtil.getToken() ?: ""
}
