package com.gitlab.plugin.api

import com.gitlab.plugin.api.duo.DuoClient
import io.ktor.client.plugins.api.*

val GitlabStatusPlugin = createClientPlugin("GitLabStatusPlugin", ::GitLabStatusPluginConf) {
  val callback = pluginConfig.statusCallback

  onRequest { _, _ -> callback.onRequestStart() }
  onResponse { response -> if (response.status.value < 400) callback.onRequestSuccess() }
}

class GitLabStatusPluginConf {
  var statusCallback: DuoClient.DuoClientRequestListener = object : DuoClient.DuoClientRequestListener {}
}
