package com.gitlab.plugin.codesuggestions.telemetry

data class Event(val type: Type, val context: Context) {
  enum class Type {
    ERROR,
    REQUEST,
    ACCEPT
  }

  data class Context(
    val modelEngine: String = "",
    val modelName: String = "",
    val language: String = ""
  )
}
