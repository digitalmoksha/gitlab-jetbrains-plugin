package com.gitlab.plugin.codesuggestions.telemetry

import com.intellij.openapi.diagnostic.Logger

class LogDestination(private val logger: Logger = Logger.getInstance(LogDestination::class.java)) :
  Telemetry.Destination {
  override fun event(event: Event) {
    logger.info("Type: ${event.type}, Context: ${event.context}")
  }
}
