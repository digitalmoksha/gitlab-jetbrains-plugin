package com.gitlab.plugin.codesuggestions.telemetry


class Telemetry(private val destinations: List<Destination> = listOf(LogDestination())) {
  interface Destination {
    fun event(event: Event)
  }

  companion object {
    val instance by lazy { Telemetry() }
  }

  fun error(context: Event.Context) = broadcast(Event(Event.Type.ERROR, context))
  fun request(context: Event.Context) = broadcast(Event(Event.Type.REQUEST, context))
  fun accept(context: Event.Context) = broadcast(Event(Event.Type.ACCEPT, context))

  private fun broadcast(event: Event) {
    destinations.forEach {
      it.event(event)
    }
  }
}
