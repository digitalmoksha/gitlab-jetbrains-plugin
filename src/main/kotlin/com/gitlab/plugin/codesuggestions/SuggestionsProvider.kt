package com.gitlab.plugin.codesuggestions

import com.gitlab.plugin.api.duo.DuoApi
import com.gitlab.plugin.api.duo.requests.Completion
import com.gitlab.plugin.util.GitLabUtil
import com.intellij.codeInsight.inline.completion.InlineCompletionContext.Companion.initOrGetInlineCompletionContext
import com.intellij.codeInsight.inline.completion.InlineCompletionElement
import com.intellij.codeInsight.inline.completion.InlineCompletionProvider
import com.intellij.codeInsight.inline.completion.InlineCompletionRequest
import com.intellij.openapi.editor.event.DocumentEvent
import org.jetbrains.annotations.ApiStatus

@ApiStatus.Experimental
class SuggestionsProvider : InlineCompletionProvider {
  private val api: DuoApi by lazy { DuoApi.instance }

  override suspend fun getProposals(request: InlineCompletionRequest): List<InlineCompletionElement> {
    if (!GitLabUtil.isSupportedFileExtension(request.file.fileType.defaultExtension)
      || outsideCompletionBounds(request)
      || request.editor.initOrGetInlineCompletionContext().isCurrentlyDisplayingInlays
    ) {
      return emptyList()
    }

    val payload = preparePayload(request)

    val completionRequest = api.completionsLegacy(payload) ?: return emptyList()

    if (completionRequest.choices.isEmpty()) return emptyList()

    return listOf(InlineCompletionElement(completionRequest.choices[0].text))
  }

  override fun isEnabled(event: DocumentEvent): Boolean = api.isApiEnabled()

  private fun preparePayload(request: InlineCompletionRequest): Completion.Payload {
    val document = request.document
    val content = document.charsSequence
    val currentPosition: Int = request.startOffset + 1

    val aboveCursor = content.take(currentPosition).toString()
    val belowCursor = content.drop(currentPosition).toString()

    return Completion.Payload(
      currentFile = Completion.Payload.CodeSuggestionRequestFile(
        request.file.name,
        aboveCursor,
        belowCursor
      ),
      telemetry = emptyList()
    )
  }

  private fun outsideCompletionBounds(request: InlineCompletionRequest): Boolean {
    val document = request.document
    val content = document.charsSequence
    val currentPosition: Int = request.startOffset + 1

    return document.textLength > content.length
        || currentPosition < 0
        || currentPosition > document.textLength
  }
}
