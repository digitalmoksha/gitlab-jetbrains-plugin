package com.gitlab.plugin.ui

import com.gitlab.plugin.api.GitLabOfflineException
import com.gitlab.plugin.api.GitLabUnauthorizedException
import com.gitlab.plugin.ui.CreateNotificationExceptionHandler.Companion.OFFLINE_NOTIFICATION_MESSAGE
import com.gitlab.plugin.ui.CreateNotificationExceptionHandler.Companion.UNAUTHORIZED_NOTIFICATION_MESSAGE
import com.intellij.openapi.diagnostic.Logger
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import kotlin.test.assertFailsWith

class CreateNotificationExceptionHandlerTest {
  private val notificationManagerMock = mockk<GitLabNotificationManager>()
  private val loggerMock = mockk<Logger>()
  private val exceptionHandler = CreateNotificationExceptionHandler(notificationManagerMock, loggerMock)
  private val unauthorizedNotification = Notification("GitLab", UNAUTHORIZED_NOTIFICATION_MESSAGE)
  private val offlineNotification = Notification("GitLab", OFFLINE_NOTIFICATION_MESSAGE)
  private val unauthorizedException = mockk<GitLabUnauthorizedException>()
  private val offlineException = mockk<GitLabOfflineException>()

  @BeforeEach
  fun beforeEach() {
    every { notificationManagerMock.sendNotification(any()) } returns Unit
    every { loggerMock.error(any<Throwable>()) } returns Unit
  }

  @Nested
  inner class HandleException {
    @Test
    fun `GitlabOfflineException creates notification with unauthorized message only once`() {
      exceptionHandler.handleException(offlineException)
      exceptionHandler.handleException(offlineException)

      verify(exactly = 1) { notificationManagerMock.sendNotification(offlineNotification) }
    }

    @Test
    fun `GitLabUnauthorizedException creates notification with unauthorized message only once`() {
      exceptionHandler.handleException(unauthorizedException)
      exceptionHandler.handleException(unauthorizedException)

      verify(exactly = 1) { notificationManagerMock.sendNotification(unauthorizedNotification) }
    }

    @Test
    fun `Other exceptions do not trigger notifications`() {
      try {
        exceptionHandler.handleException(Exception())
      } catch (ex: Exception) {
        verify(exactly = 0) { notificationManagerMock.sendNotification(any()) }
      }
    }

    @Test
    fun `throws non-handled exceptions`() {
      assertFailsWith<RuntimeException> { exceptionHandler.handleException(RuntimeException()) }
    }
  }

  @Nested
  inner class ResetNotifications {
    @Test
    fun `unauthorized notifications can be send again after reset`() {
      exceptionHandler.handleException(unauthorizedException)

      exceptionHandler.resetNotifications()

      exceptionHandler.handleException(unauthorizedException)

      verify(exactly = 2) { notificationManagerMock.sendNotification(unauthorizedNotification) }
    }

    @Test
    fun `offline notifications can be send again after reset`() {
      exceptionHandler.handleException(offlineException)

      exceptionHandler.resetNotifications()

      exceptionHandler.handleException(offlineException)

      verify(exactly = 2) { notificationManagerMock.sendNotification(offlineNotification) }
    }
  }
}
