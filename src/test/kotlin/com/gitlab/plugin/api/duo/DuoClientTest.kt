package com.gitlab.plugin.api.duo

import com.gitlab.plugin.api.GitLabOfflineException
import com.gitlab.plugin.api.GitLabUnauthorizedException
import com.gitlab.plugin.ui.CreateNotificationExceptionHandler
import io.ktor.client.call.*
import io.ktor.client.engine.*
import io.ktor.client.engine.mock.*
import io.ktor.client.network.sockets.*
import io.ktor.client.plugins.*
import io.ktor.client.request.*
import io.ktor.client.statement.*
import io.ktor.http.*
import io.ktor.utils.io.*
import io.mockk.*
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.runTest
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertFalse
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import java.nio.channels.UnresolvedAddressException
import kotlin.test.assertFailsWith

class DuoClientTest {
  private val exceptionHandlerMock = mockk<CreateNotificationExceptionHandler>()
  val mockListener = mockk<DuoClient.DuoClientRequestListener>()

  @BeforeEach
  fun init() {
    clearMocks(exceptionHandlerMock, mockListener)

    every { exceptionHandlerMock.handleException(any()) } returns Unit
    every { mockListener.onRequestStart() } returns Unit
    every { mockListener.onRequestError() } returns Unit
    every { mockListener.onRequestSuccess() } returns Unit
  }

  @Nested
  inner class IsEnabled {
    @Test
    fun `when PAT token is empty`() {
      val client = buildClient(tokenProvider = { "" })
      assertFalse(client.isEnabled())
    }

    @Test
    fun `when PAT token is not empty`() {
      val client = buildClient(tokenProvider = { "some_token" })

      assert(client.isEnabled())
    }
  }

  @Nested
  inner class RestClient {
    @Test
    fun `builds request headers correctly`() = runTest {
      val pat = "VALID_PAT"

      val requestData = captureRequest { mockEngine ->
        buildClient(httpClientEngine = mockEngine, tokenProvider = { pat })
          .restClient
          .get("someEndpoint")
      }.first

      assertEquals("application/json", requestData.headers[HttpHeaders.ContentType])
      assertEquals("gitlabUserAgent", requestData.headers[HttpHeaders.UserAgent])
    }

    @Test
    fun `converts response object keys from underscore`() = runTest {
      val body = buildClient(httpClientEngine = buildMockEngine(content = """{"some_property": "abc"}"""))
        .restClient
        .get("someEndpoint")
        .body<Response>()

      assertEquals(Response(someProperty = "abc"), body)
    }

    @Test
    fun `throws GitLabOfflineException on UnresolvedAddressException`() {
      val mockEngine = MockEngine { _ -> throw UnresolvedAddressException() }

      assertFailsWith<GitLabOfflineException> {
        runBlocking {
          buildClient(httpClientEngine = mockEngine)
            .restClient
            .get("some_endpoint")
        }
      }
    }

    @Test
    fun `throws GitLabOfflineException on ConnectTimeoutException`() {
      val mockEngine = MockEngine { _ -> throw ConnectTimeoutException("timeout") }

      val client = buildClient(httpClientEngine = mockEngine).restClient

      assertFailsWith<GitLabOfflineException> {
        runBlocking { client.get("some_endpoint") }
      }
    }

    @Test
    fun `throws GitLabUnauthorizedException on unauthorized`() = runTest {
      val mockEngine = buildMockEngine(HttpStatusCode.Unauthorized, "You are not logged in")

      val client = buildClient(httpClientEngine = mockEngine)

      assertFailsWith<GitLabUnauthorizedException>("You are not logged in") {
        client.get("some_endpoint")
      }
    }

    @Test
    fun `calls listener methods on request success`() = runTest {
      buildClient(onStatusChanged = mockListener)
        .restClient
        .get("someEndpoint")

      verifySequence {
        mockListener.onRequestStart()
        mockListener.onRequestSuccess()
      }
    }

    @Test
    fun `calls listener methods on request error`() = runTest {
      val mockEngine = buildMockEngine(HttpStatusCode.BadRequest)

      try {
        buildClient(httpClientEngine = mockEngine, onStatusChanged = mockListener).restClient.get("someEndpoint")
      } catch (ex: ClientRequestException) {
        // expected exception here since it's BadRequest
      }

      verifySequence {
        mockListener.onRequestStart()
        mockListener.onRequestError()
      }
    }
  }

  @Nested
  inner class Get {
    @Test
    fun `creates the request passing additional options`() = runTest {
      val pat = "a_pat"

      val requestData = captureRequest { mockEngine ->
        buildClient(httpClientEngine = mockEngine, tokenProvider = { pat })
          .get("someEndpoint") {
            header("SOME_HEADER", "SOME_VALUE")
          }
      }.first

      assertEquals(HttpMethod.Get, requestData.method)
      assertEquals("SOME_VALUE", requestData.headers["SOME_HEADER"])
      assertEquals("Bearer $pat", requestData.headers[HttpHeaders.Authorization])
    }

    @Test
    fun `handles exception using the exception handler`() = runTest {
      buildClient(
        httpClientEngine = buildMockEngine(HttpStatusCode.BadRequest),
        exceptionHandler = exceptionHandlerMock
      )
        .get("someEndpoint")

      verify(exactly = 1) { exceptionHandlerMock.handleException(ofType<ClientRequestException>()) }
    }
  }

  @Nested
  inner class Post {
    @Test
    fun `creates the request passing additional options`() = runTest {
      val pat = "a_pat"

      val requestData = captureRequest { mockEngine ->
        buildClient(httpClientEngine = mockEngine, tokenProvider = { pat })
          .post("someEndpoint") {
            header("SOME_HEADER", "SOME_VALUE")
          }
      }.first

      assertEquals(HttpMethod.Post, requestData.method)
      assertEquals("SOME_VALUE", requestData.headers["SOME_HEADER"])
      assertEquals("Bearer $pat", requestData.headers[HttpHeaders.Authorization])
    }

    @Test
    fun `handles exception using the exception handler`() = runTest {
      buildClient(
        httpClientEngine = buildMockEngine(HttpStatusCode.BadRequest),
        exceptionHandler = exceptionHandlerMock
      )
        .post("someEndpoint")

      verify(exactly = 1) { exceptionHandlerMock.handleException(ofType<ClientRequestException>()) }
    }
  }

  @Nested
  inner class ModelGatewayPost {
    @Test
    fun `overwrites authentication and url when creating the request`() = runTest {
      val requestData = captureRequest { mockEngine ->
        buildClient(httpClientEngine = mockEngine, jwtProvider = { "jwt" }, tokenProvider = { "pat" })
          .modelGatewayPost {
            header("SOME_HEADER", "SOME_VALUE")
          }
      }.first

      assertEquals(HttpMethod.Post, requestData.method)
      assertEquals(DuoClient.MODEL_GATEWAY_ENDPOINT, requestData.url.toString())
      assertEquals("SOME_VALUE", requestData.headers["SOME_HEADER"])
      assertEquals("Bearer jwt", requestData.headers[HttpHeaders.Authorization])
      assertEquals("oidc", requestData.headers["X-Gitlab-Authentication-Type"])
    }

    @Test
    fun `handles exception using the exception handler`() = runTest {
      buildClient(
        httpClientEngine = buildMockEngine(HttpStatusCode.BadRequest),
        exceptionHandler = exceptionHandlerMock
      )
        .post("someEndpoint")

      verify(exactly = 1) { exceptionHandlerMock.handleException(ofType<ClientRequestException>()) }
    }
  }

  private fun buildClient(
    tokenProvider: DuoClient.TokenProvider = DuoClient.TokenProvider { "some_pat" },
    jwtProvider: DuoClient.TokenProvider = DuoClient.TokenProvider { "some_jwt" },
    exceptionHandler: ExceptionHandler = object : ExceptionHandler {},
    httpClientEngine: HttpClientEngine = buildMockEngine(),
    userAgent: String = "gitlabUserAgent",
    onStatusChanged: DuoClient.DuoClientRequestListener = object : DuoClient.DuoClientRequestListener {},
    shouldRetry: Boolean = false
  ) = DuoClient(tokenProvider, jwtProvider, exceptionHandler, userAgent, shouldRetry, httpClientEngine, onStatusChanged)

  private fun captureRequest(block: suspend (httpEngine: HttpClientEngine) -> HttpResponse?):
    Pair<HttpRequestData, HttpResponse?> = runBlocking {
    var data: HttpRequestData? = null

    val mockEngine = MockEngine { request ->
      data = request
      respond(content = ByteReadChannel(""))
    }

    val httpResponse = block.invoke(mockEngine)

    val requestData = data ?: throw RuntimeException("Request data not found")

    return@runBlocking Pair(requestData, httpResponse)
  }

  private fun buildMockEngine(
    statusCode: HttpStatusCode = HttpStatusCode.OK,
    content: String = """{"some_property": "abc"}"""
  ): HttpClientEngine = MockEngine { request ->
    respond(
      content = ByteReadChannel(content),
      status = statusCode,
      headers = headersOf(HttpHeaders.ContentType, "application/json")
    )
  }

  private data class Response(val someProperty: String)
}
