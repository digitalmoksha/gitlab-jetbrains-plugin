package com.gitlab.plugin.api.duo

import com.gitlab.plugin.api.duo.requests.Completion
import io.ktor.client.engine.mock.*
import io.ktor.http.*
import io.ktor.utils.io.*
import io.mockk.coVerify
import io.mockk.every
import io.mockk.mockk
import kotlinx.coroutines.test.runTest
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNull
import org.junit.jupiter.api.Test

class DuoApiTest {
  private val mockPayload = mockk<Completion.Payload>()

  @Test
  fun `generates suggestions`() = runTest {
    val mockEngine = MockEngine { request ->
      respond(
        content = ByteReadChannel("""{"choices":[{"text":"text1"},{"text":"text2"}],"model":{}}"""),
        status = HttpStatusCode.OK,
        headers = headersOf(HttpHeaders.ContentType, "application/json")
      )
    }

    val duoClient = DuoClient(
      jwtProvider = { "someJwt" },
      tokenProvider = { "somePat" },
      httpClientEngine = mockEngine,
      userAgent = "someAgent",
      onStatusChanged = object : DuoClient.DuoClientRequestListener {}
    )

    val api = DuoApi(duoClient)

    val suggestions = api.completionsLegacy(mockk<Completion.Payload>())

    val expectedResponse = Completion.Response(
      choices = listOf(
        Completion.Response.Choice("text1"),
        Completion.Response.Choice("text2"),
      ),
      model = Completion.Response.Model()
    )

    assertEquals(suggestions, expectedResponse)
  }

  @Test
  fun `does not generate suggestion if api is disabled`() = runTest {
    val client = mockk<DuoClient>()
    val duoApi = DuoApi(client)

    every { client.isEnabled() } returns false

    assertNull(duoApi.completionsLegacy(mockPayload))

    coVerify(exactly = 0) { client.modelGatewayPost(any()) }
  }
}
