package com.gitlab.plugin.codesuggestions.telemetry

import com.intellij.openapi.diagnostic.Logger
import io.mockk.every
import io.mockk.mockk
import io.mockk.slot
import io.mockk.verify
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

class LogDestinationTest {
  private val logMessageSlot = slot<String>()
  private val mockLogger: Logger = mockk { every { info(capture(logMessageSlot)) } returns Unit }
  private val logDestination = LogDestination(mockLogger)

  @Test
  fun `event logs the event and model`() {
    logDestination.event(Event(Event.Type.ERROR, Event.Context(modelEngine = "test-engine")))

    verify(exactly = 1) { mockLogger.info(any<String>()) }
    assertThat(logMessageSlot.captured).contains("Type: ERROR, Context: Context(modelEngine=test-engine")
  }
}
