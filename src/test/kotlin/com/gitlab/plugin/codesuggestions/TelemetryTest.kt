package com.gitlab.plugin.codesuggestions

import com.gitlab.plugin.codesuggestions.telemetry.Event
import com.gitlab.plugin.codesuggestions.telemetry.Telemetry
import io.mockk.every
import io.mockk.mockk
import io.mockk.verifyAll
import org.junit.jupiter.api.Test

private val context = Event.Context(modelEngine = "test-engine")

class TelemetryTest {
  private val mockDestination1: Telemetry.Destination = mockk { every { event(any()) } returns Unit }
  private val mockDestination2: Telemetry.Destination = mockk { every { event(any()) } returns Unit }
  private val telemetry = Telemetry(listOf(mockDestination1, mockDestination2))

  @Test
  fun `error calls every destination with the correct type and model`() {
    telemetry.error(context)

    verifyAll {
      mockDestination1.event(Event(Event.Type.ERROR, context))
      mockDestination2.event(Event(Event.Type.ERROR, context))
    }
  }

  @Test
  fun `request calls every destination with the correct type and model`() {
    telemetry.request(context)

    verifyAll {
      mockDestination1.event(Event(Event.Type.REQUEST, context))
      mockDestination2.event(Event(Event.Type.REQUEST, context))
    }
  }

  @Test
  fun `accept calls every destination with the correct type and model`() {
    telemetry.accept(context)

    verifyAll {
      mockDestination1.event(Event(Event.Type.ACCEPT, context))
      mockDestination2.event(Event(Event.Type.ACCEPT, context))
    }
  }
}

